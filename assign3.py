import argparse
import re
import operator

parser = argparse.ArgumentParser(description='run ---> python main.py Aladdin.txt')
# parser.add_argument('--logdir', dest='logdir', action='store', default='/home/timelighter/save/osim_rl')

parser.add_argument("file")
arg = parser.parse_args()
# spec_chars = ['\t', '\r', '\n', ' ', '"', ':', ',', ';', '`', "(", ")", "!", "?", "{", "}", ".", '\\']
filepath = arg.file

def count_words(filepath) :
    word_count = {}
    # sentence to words
    with open(filepath,'r') as f:
        for line in f:
            for word in line.split(" "):
                new_word = re.sub('\s', '', word)
                new_word = re.sub('>', '', new_word)
                new_word = re.sub('<', '', new_word)
                new_word = re.sub('{', '', new_word)
                new_word = re.sub('}', '', new_word)
                ## check for symbols and discard them if any,
                new_word = re.sub(r"""["?,$!]|'(?!(?<! ')[ts])""", "", new_word)
                new_word = re.sub(r"\n|\r|\.|\)|\(|:|-| ",  "", new_word)

                ## uppercase to lowercase
                # print new_word
                new_word = re.sub(r'^.*([A-Z].*)$', lambda m: m.group(0).lower(), new_word)

                if new_word == '' : # Dealing with bugs..
                    continue

                ## append to dictionary with counts
                if new_word in word_count :
                    word_count[new_word] += 1
                else :
                    word_count[new_word] = 1
    return word_count

def draw_line_graph_html(sorted_words) :
    import matplotlib.pyplot as plt
    words = []
    frequency = []
    just_the_rank = []
    for idx, dict in enumerate(sorted_words) :
        words.append(dict[0])
        frequency.append(dict[1])
        just_the_rank.append(idx)
    #plt.loglog(just_the_rank, frequency, basex=10)
    plt.plot(just_the_rank, frequency, color='orange')

    plt.xlabel('words')
    plt.ylabel('frequency')
    plt.show()


def main(filepath) :
    word_count = count_words(filepath)
    sorted_words = sorted(word_count.items(), key=operator.itemgetter(1), reverse=True)
    # print(len(sorted_words))
    # sorted_words[len(sorted_words)]
    # for i in sorted_words :
    #     print i[0], ' : ', i[1]
    #

    draw_line_graph_html(sorted_words)
if __name__ == "__main__":
    # filepath = arg.file
    main(filepath)